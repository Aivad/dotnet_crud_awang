﻿using System.Threading.Tasks;

namespace awang_dotnet_task.Model
{
    public class UserModel
    {
       
        public int pk_users_id { get; set; }

        public string? name { get; set; }

        public List<TaskModel>? tasks { get; set; }

    }
}
